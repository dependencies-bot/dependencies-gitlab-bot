FROM ruby:2.7-slim-buster

LABEL maintainer="zedtux@zedroot.org"

ENV DEBIAN_FRONTEND noninteractive

# ~~~~ System locales ~~~~
RUN apt-get update && apt-get install -y locales && \
  dpkg-reconfigure locales && \
  locale-gen C.UTF-8 && \
  /usr/sbin/update-locale LANG=C.UTF-8 && \
  echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && \
  locale-gen

# Set default locale for the environment
ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV APP_HOME /application

# ~~~~ User and folders ~~~~
RUN useradd -ms /bin/bash dependencies-gitlab-bot && \
  mkdir $APP_HOME && \
  chown -R dependencies-gitlab-bot:dependencies-gitlab-bot $APP_HOME

# ~~~~ Dependencies ~~~~
RUN apt-get update -qq && \
    apt-get install -y git

# ~~~~ Bundler ~~~~
RUN gem install bundler \
    && gem update --system \
    && bundle --version \
    && gem list

# ~~~~ Install Rails application gems ~~~~
ENV BUNDLE_GEMFILE=$APP_HOME/Gemfile \
    BUNDLE_JOBS=8

WORKDIR $APP_HOME
COPY --chown=dependencies-gitlab-bot:dependencies-gitlab-bot Gemfile* $APP_HOME/

RUN bundle install \
    && bundle update --bundler \
    && cat Gemfile.lock

# ~~~~ Import application ~~~~
COPY --chown=dependencies-gitlab-bot:dependencies-gitlab-bot . $APP_HOME

USER dependencies-gitlab-bot

ENTRYPOINT ["bundle", "exec"]
CMD ["rake", "-T"]
