module Dependencies
  module GitlabBot
    class Helpers
      def self.build_merge_request_title_for(gem_name, attributes)
        version = build_next_version(attributes)
        "Bump #{gem_name} from #{attributes[:current_version]} to #{version}"
      end

      #
      # Returns the merge request title.
      #
      # @param gem_name [String] Name of the gem
      # @param from [String] Version from which the update is made
      # @param next [String] Version to which the update is made
      # @return [String] Merge request title
      #
      def self.build_merge_request_title_from(gem_name, from, to)
        "Bump #{gem_name} from #{from} to #{to}"
      end

      def self.build_next_version(attributes)
        attributes[:updates][build_update_kind(attributes)]
      end

      def self.build_update_kind(attributes)
        if attributes[:updates].key?(:soft)
          :soft
        else
          :hard
        end
      end

      def self.namespace
        'dependencies-gitlab-bot/bundler'
      end
    end
  end
end
