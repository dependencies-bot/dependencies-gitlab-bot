module YamlValidator
  def self.validate!(yaml)
    raise ArgumentError, 'YAML should be a Hash' unless yaml.is_a?(Hash)

    ensure_yaml_projects_node_present!(yaml)

    ensure_yaml_projects_is_an_array!(yaml)

    projects = yaml['projects'].compact

    ensure_project_list_is_valid!(projects)
  end

  def self.ensure_yaml_projects_node_present!(yaml)
    return true unless yaml['projects'].nil? && yaml['projects'].empty?

    raise ArgumentError, 'Missing the projects node'
  end

  def self.ensure_yaml_projects_is_an_array!(yaml)
    return true if yaml['projects'].is_a?(Array)

    raise ArgumentError, 'YAML projects node should be an Array'
  end

  def self.ensure_project_list_is_valid!(projects)
    raise ArgumentError, 'YAML projects is empty' if projects.empty?

    invalid_ids = projects.map do |project_id|
      project_id if project_id.to_i == 0
    end.compact

    unless invalid_ids.empty?
      raise ArgumentError,
            "Invalid project IDs found: #{invalid_ids.join(',')}"
    end
  end
end
