require 'interactors/dependencies_gitlab_bot/parse_projects_yaml_file_and_collect_project_ids'
require 'interactors/dependencies_gitlab_bot/trigger_gems_update_process_for_each_project_id'

class OrganizePostingMergeRequestsToUpdateProjectsGems
  include Interactor::Organizer

  organize DependenciesGitlabBot::ParseProjectsYamlFileAndCollectProjectIds,
           DependenciesGitlabBot::TriggerGemsUpdateProcessForEachProjectId
end
