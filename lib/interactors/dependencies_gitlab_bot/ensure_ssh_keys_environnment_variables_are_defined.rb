module DependenciesGitlabBot
  class EnsureSshKeysEnvironnmentVariablesAreDefined
    include Interactor

    def call
      if ENV['HOME'].nil? || ENV['HOME'].empty?
        context.fail!(error: { HOME: 'env is missing' })
      end

      if ENV['SSH_PRIVATE_KEY'].nil? || ENV['SSH_PRIVATE_KEY'].empty?
        context.fail!(error: { SSH_PRIVATE_KEY: 'env is missing' })
      end

      context.ssh_private_key = ENV['SSH_PRIVATE_KEY'].gsub('\'', '')
                                                      .gsub('\n', "\n") + "\n"
    end
  end
end
