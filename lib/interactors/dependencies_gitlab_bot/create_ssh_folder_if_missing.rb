module DependenciesGitlabBot
  class CreateSshFolderIfMissing
    include Interactor

    def call
      context.ssh_folder_path = File.join(ENV['HOME'], '.ssh')

      return if File.directory?(context.ssh_folder_path)

      FileUtils.mkdir(context.ssh_folder_path)
    end
  end
end
