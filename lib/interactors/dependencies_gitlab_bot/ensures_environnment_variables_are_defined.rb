module DependenciesGitlabBot
  class EnsuresEnvironnmentVariablesAreDefined
    include Interactor

    def call
      if ENV['BOT_EMAIL'].nil? || ENV['BOT_EMAIL'].empty?
        context.fail!(error: { BOT_EMAIL: 'env is missing' })
      end

      if ENV['BOT_NAME'].nil? || ENV['BOT_NAME'].empty?
        context.fail!(error: { BOT_NAME: 'env is missing' })
      end

      if ENV['BOT_NAMESPACE'].nil? || ENV['BOT_NAMESPACE'].empty?
        context.fail!(error: { BOT_NAMESPACE: 'env is missing' })
      end

      if ENV['CLONE_PATH'].nil? || ENV['CLONE_PATH'].empty?
        context.fail!(error: { CLONE_PATH: 'env is missing' })
      end
    end
  end
end
