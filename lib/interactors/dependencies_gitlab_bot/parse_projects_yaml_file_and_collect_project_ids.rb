require 'yaml'

require 'yaml_helpers'
require 'yaml_validator'

module DependenciesGitlabBot
  class ParseProjectsYamlFileAndCollectProjectIds
    include Interactor

    def call
      ensure_root_path_well_defined!

      yaml = YAML.load_file(YamlHelpers.project_yml_file_path)
      YamlValidator.validate!(yaml)

      context.project_ids = yaml['projects']
      project_ids_to_s = context.project_ids.join(', ')
      puts "[#{self.class.name}] Gitlab project IDs: #{project_ids_to_s}"
    end

    private

    def ensure_root_path_well_defined!
      if ENV['ROOT_PATH'].nil? || ENV['ROOT_PATH'].empty?
        context.fail!(error: 'ROOT_PATH env variable is missing')
      end

      return if File.exist?(ENV['ROOT_PATH'])

      context.fail!(error: 'ROOT_PATH does not exists')
    end
  end
end
