require 'bundler/injector'

require_relative '../git/revert_changes'

module Bundler
  class UpdateGemfileLock
    include Interactor

    def call
      sanity_checks!

      puts "[#{self.class.name}] Building Bundler definition ..."

      builder = Bundler::Dsl.new
      builder.eval_gemfile(context.local_project_gemfile_path)

      definition = builder.to_definition(context.project_gemfilelock_path,
                                         gems: [context.gem_name])

      puts "[#{self.class.name}] Resolving dependencies remotely ..."
      # Update the definition with new requirements from updated gem
      definition.resolve_remotely!

      puts "[#{self.class.name}] Locking #{context.project_gemfilelock_path} " \
           '...'
      # Write the update to the Gemfile.lock
      definition.lock(context.project_gemfilelock_path)
    rescue Bundler::VersionConflict => error
      # The gem update command fails due to conflicts (another gem depends on
      # another version of the current gem) so we do nothing, just continuing
      # with the next gems so that later the conflict will be fixed and this
      # will pass.

      # Revert changes so that it will not be kept for the next gem update.
      revert = Git::RevertChanges.call(local_git_repo: context.local_git_repo)

      if revert.failure?
        puts "[#{self.class.name}] ERROR: Reverting changes failed: " \
             "#{revert.error}"
      end

      context.fail!(error: 'Unresolvable conflict, abandonning to ' \
                           "update the gem #{context.gem_name}.\nError " \
                           "message was:\n#{error.message}")
    rescue Bundler::GemNotFound => error
      # Display the Gemfile content before to re-raise the error
      puts "[#{self.class.name}] An error occured while trying to update the " \
           "gem #{context.gem_name}: #{error.message}."
      puts "[#{self.class.name}] See the original Gemfile:\n" \
           "#{context.original_gemfile}"
      puts "[#{self.class.name}] See the updated Gemfile:\n" \
           "#{File.read(context.local_project_gemfile_path)}"

      raise
    end

    private

    def sanity_checks!
      context.fail!(error: { gem_name: 'is missing' }) unless context.gem_name

      unless context.local_project_gemfile_path
        context.fail!(error: { local_project_gemfile_path: 'is missing' })
      end

      return if context.project_gemfilelock_path

      context.fail!(error: { project_gemfilelock_path: 'is missing' })
    end
  end
end
