require 'bundler/shared_helpers'

module Bundler
  class UpdateGemfile
    include Interactor

    def call
      sanity_checks!

      context.original_gemfile = File.read(context.local_project_gemfile_path)

      new_gemfile = IO.readlines(context.local_project_gemfile_path)
      new_gemfile = update_gemfile(new_gemfile)
      write_gemfile(new_gemfile)

      context.gemfile_updated = true
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      return if context.local_project_gemfile_path

      context.fail!(error: { local_project_gemfile_path: 'is missing' })
    end

    def build_regex_detect_gem
      /gem\s+['"]#{context.gem_name}['"]/
    end

    def build_regex_extract_version
      /gem\s*['"]#{context.gem_name}['"]\s*,\s*['"](?:~>|>=|=)?\s*([\d\.\-A-z]+)['"]/
    end

    #
    # Group 1: Catches the gem 'gem name'
    # Group 2: Catches single or double quote before the first version
    # Group 3: Catches the version selector (~> >= = <=)
    # Group 4: Catches the version number
    # Group 5: Catches single or double quote after the first version
    # Group 6: Catches the trailing comma if any
    #
    def build_regex_replace_version
      /(gem\s*['"][A-z\d\-_]+['"],)\s*(['"])([<~>=]+)?\s*([\d\.\-A-z]+)(['"])(,)?/
    end

    def extract_version_from(line)
      line.scan(build_regex_extract_version).flatten
    end

    def update_gem_version(line)
      updated_line = line.sub(build_regex_replace_version) do
        "#{$1} #{$2}#{$3} #{context.next_version}#{$5}#{$6}"
      end

      updated_line[-1] == "\n" ? updated_line : updated_line + "\n"
    end

    def update_gemfile(gemfile)
      # Using `each_with_index` over `.each.with_index`
      # https://stackoverflow.com/a/20258604/1996540
      gemfile.each_with_index do |line, index|
        # Search the line for the current gem
        if gemfile[index] =~ build_regex_detect_gem
          version = extract_version_from(gemfile[index]).first
          puts "[#{self.class.name}] Gem found with version #{version}. " \
               'Updating ...'
          before = gemfile[index]
          gemfile[index] = update_gem_version(gemfile[index])
          puts "[#{self.class.name}] Updated #{before.inspect} to " \
               "#{gemfile[index].inspect}"
        end
      end

      gemfile.join.chomp
    end

    def write_gemfile(gemfile)
      gemfile_path = context.local_project_gemfile_path

      # TODO : Replace with `Bundler::SharedHelpers.write_to_gemfile` when
      # available.
      Bundler::SharedHelpers.filesystem_access(gemfile_path) do |g|
        File.open(g, "w") { |file| file.puts gemfile }
      end
    end
  end
end
