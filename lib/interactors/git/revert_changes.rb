module Git
  class RevertChanges
    include Interactor

    def call
      sanity_checks!

      context.git_branch = checkout_branch
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from reverting changes ' \
                           "in the git repository: #{error.message}")
    end

    private

    def sanity_checks!
      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def checkout_branch
      puts "[#{self.class.name}] Reverting changes in the current branch ..."

      puts "[#{self.class.name}] #{context.local_git_repo.reset_hard(
        context.local_git_repo.gcommit('HEAD^')
      )}"

      puts "[#{self.class.name}] #{context.local_git_repo.status.inspect}"
    end
  end
end
