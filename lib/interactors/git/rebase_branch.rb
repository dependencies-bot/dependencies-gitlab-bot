module Git
  #
  # When a merge request has a `merge_state` as `cannot_be_merged`, it could be
  # that there are conflicts, which could come from a previous gem update.
  # Here we are rebasing the branch with master in order to try to resolve
  # conflicts.
  #
  class RebaseBranch
    include Interactor

    def call
      sanity_checks!

      checkout_gem_update_branch && rebase && push_force
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from rebasing the branch ' \
                           "#{context.git_branch.name}: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.git_branch
        context.fail!(error: { git_branch: 'is missing', source: 'RebaseBranch' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def checkout_gem_update_branch
      puts "[#{self.class.name}] Checking out the #{context.git_branch.name} " \
           "branch ..."
      puts context.local_git_repo.branch(context.git_branch.name).checkout
      true
    end

    def push_force
      puts "[#{self.class.name}] Force pushing the #{context.git_branch.name}" \
           " branch ..."
      puts context.local_git_repo.push('origin', context.git_branch.name, force: true)
      true
    end

    def rebase
      puts "[#{self.class.name}] Rebasing " \
           "#{context.local_git_repo.current_branch} with the master branch ..."
      puts context.local_git_repo.rebase('master', strategy_option: 'ours')
      true
    end
  end
end
