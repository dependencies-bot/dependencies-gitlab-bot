module Git
  #
  # In order to rebase a branch, an upstream remote is required in order to
  # update the master branch of our fork from the upstream.
  # This class checks and add, if missing, a remote named upstream to the forked
  # repository URL.
  #
  class AddUpstreamWhenMissing
    include Interactor

    def call
      sanity_checks!

      are_remotes_missing_upstream? && create_upstream_remote
      context.upstream_remote = select_upstream_remote
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from rebasing the branch ' \
                           "#{context.git_branch.name}: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.remote_project
        context.fail!(error: { remote_project: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def are_remotes_missing_upstream?
      select_upstream_remote == nil
    end

    def create_upstream_remote
      puts "[#{self.class.name}] Adding the upstream remote pointing to the " \
           "#{context.remote_project.ssh_url_to_repo} URL ..."
      context.local_git_repo.add_remote(
        'upstream',
        context.remote_project.ssh_url_to_repo
      )
    end

    def select_upstream_remote
      @upstream ||= context.local_git_repo.remotes.detect do |remote|
        puts "[#{self.class.name}] Remote #{remote.name} pointing to " \
             "#{remote.url}"
        remote.name == 'upstream'
      end
    end
  end
end
