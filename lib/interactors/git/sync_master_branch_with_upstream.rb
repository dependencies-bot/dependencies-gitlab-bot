module Git
  class SyncMasterBranchWithUpstream
    include Interactor

    def call
      sanity_checks!

      sync_master_branch_with_upstream && push
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from syncing the master ' \
                           "branch with upstream: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.upstream_remote
        context.fail!(error: { upstream_remote: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def push
      puts "[#{self.class.name}] Pushing the " \
           "#{context.local_git_repo.current_branch} branch ..."
      context.local_git_repo.push(
        'origin',
        context.local_git_repo.current_branch
      )
    end

    def sync_master_branch_with_upstream
      puts "[#{self.class.name}] Merging " \
           "#{context.upstream_remote.name}/master with " \
           "#{context.local_git_repo.current_branch} branch ..."
      puts context.upstream_remote.merge('master')
      true
    end
  end
end
