module Git
  #
  # Checkout the master branch, and run the pull command
  #
  class CheckoutMasterBranchAndPull
    include Interactor

    def call
      sanity_checks!

      checkout_master && pull
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from checking out and pulling' \
                           "the master branch: #{error.message}")
    end

    private

    def sanity_checks!
      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def checkout_master
      puts "[#{self.class.name}] Checking out the master branch ..."
      puts context.local_git_repo.branch('master').checkout
      true
    end

    def pull
      puts "[#{self.class.name}] Pulling the " \
           "#{context.local_git_repo.current_branch} branch ..."
      puts context.local_git_repo.pull
      true
    end
  end
end
