module Git
  class CheckoutBranch
    include Interactor

    def call
      sanity_checks!

      context.git_branch = checkout_branch
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from commiting and pushing ' \
                           "from the git repository: #{error.message}")
    end

    private

    def sanity_checks!
      unless context.git_branch_name
        context.fail!(error: { git_branch_name: 'is missing' })
      end

      unless context.git_branch_sha
        context.fail!(error: { git_branch_sha: 'is missing' })
      end

      return if context.local_git_repo

      context.fail!(error: 'Missing reference to local git repo.')
    end

    def checkout_branch
      puts "[#{self.class.name}] Checking out the #{context.git_branch_name}" \
           " branch ..."
      puts context.local_git_repo.branch(context.git_branch_name).checkout

      unless context.local_git_repo.current_branch == context.git_branch_name
        context.fail!(
          error: "Checking out of #{context.git_branch_name} didn't worked, " \
                 'the current branch is ' \
                 "#{context.local_git_repo.current_branch}"
        )
      end

      local_branch_last_commit_sha = context.local_git_repo.revparse(
        context.git_branch_name
      )

      unless local_branch_last_commit_sha == context.git_branch_sha
        reset_hard_and_re_check!(local_branch_last_commit_sha)
      end

      puts "[#{self.class.name}] #{context.local_git_repo.current_branch} branch log:"
      context.local_git_repo.log.each do |commit|
        puts "[#{self.class.name}] #{commit.sha} : #{commit.message}"
      end

      context.local_git_repo.branch(context.git_branch_name)
    end

    def reset_hard_and_re_check!(previous_commit_sha)
      puts "[#{self.class.name}] WARNING: Checked out branch last commit SHA " \
           'is different than remote branch last commit SHA! Trying a ' \
           "\`reset --hard\` ..."

      puts context.local_git_repo.reset_hard(context.git_branch_sha)

      local_branch_last_commit_sha = context.local_git_repo.revparse(
        context.git_branch_name
      )

      return if local_branch_last_commit_sha == context.git_branch_sha

      context.fail!(
        error: "Checked out branch #{context.git_branch_name} last commit " \
               "SHA (#{previous_commit_sha}) doesn't match the remote " \
               "branch last commit SHA (#{context.git_branch_sha})."
      )
    end
  end
end
