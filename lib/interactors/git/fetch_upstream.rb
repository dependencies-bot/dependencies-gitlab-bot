module Git
  class FetchUpstream
    include Interactor

    def call
      sanity_checks!

      fetch_upstream
    rescue Git::GitExecuteError => error
      context.fail!(error: 'Something prevented from fetching the upstream :' \
                           "#{error.message}")
    end

    private

    def sanity_checks!
      return if context.upstream_remote

      context.fail!(error: 'Missing reference to the upstream remote.')
    end

    def fetch_upstream
      puts "[#{self.class.name}] Fetching upstream remote ..."
      puts context.upstream_remote.fetch
    end
  end
end
