require_relative 'bundler/ensure_a_gemfile_exists'
require_relative 'bundler/fetch_gem_list_from_project_gemfile'
require_relative 'dependencies_gitlab_bot/ensures_environnment_variables_are_defined'
require_relative 'dependencies_gitlab_bot/trigger_gem_update_process_for_each_gems'
require_relative 'git/add_upstream_when_missing'
require_relative 'git/clone_repository'
require_relative 'git/fetch_upstream'
require_relative 'git/initialize_client'
require_relative 'git/sync_master_branch_with_upstream'
require_relative 'gitlab/fork_repository'
require_relative 'gitlab/initialize_client'
require_relative 'gitlab/remove_updates_where_a_pending_merge_request_exists'
require_relative 'rubygems/search_dependencies_update'

class OrganizeUpdatingProjectGems
  include Interactor::Organizer

  organize DependenciesGitlabBot::EnsuresEnvironnmentVariablesAreDefined,
           Gitlab::InitializeClient,
           Gitlab::ForkRepository,
           Git::InitializeClient,
           Git::CloneRepository,
           Git::AddUpstreamWhenMissing,
           Git::FetchUpstream,
           Git::SyncMasterBranchWithUpstream,
           Bundler::EnsureAGemfileExists,
           Bundler::FetchGemListFromProjectGemfile,
           Rubygems::SearchDependenciesUpdate,
           Gitlab::RemoveUpdatesWhereAPendingMergeRequestExists,
           DependenciesGitlabBot::TriggerGemUpdateProcessForEachGems
end
