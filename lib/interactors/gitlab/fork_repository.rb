module Gitlab
  class ForkRepository
    include Interactor

    def call
      sanity_checks!

      fetches_bot_user_namespace
      fetches_remote_project

      if context.bot_namespace_id == context.remote_project_namespace_id
        puts "[#{self.class.name}] Project's owner is the same as the bot, no forking."
        context.local_project = context.remote_project.dup
      else
        puts "[#{self.class.name}] Forking project ID #{context.id} ..."
        context.local_project = fork_remote_project
      end
    end

    private

    def sanity_checks!
      context.fail!(error: { id: 'is missing' }) unless context.id

      return if context.gitlab

      context.fail!(error: 'Gitlab client not initialized')
    end

    def fetches_bot_user_namespace
      gitlab_user = context.gitlab.user

      gitlab_namespace = context.gitlab.namespaces.detect do |namespace|
        namespace.kind == 'user' && namespace.name == gitlab_user.name
      end

      context.bot_namespace_id = gitlab_namespace.id
      context.bot_namespace_name = gitlab_namespace.name
      puts "[#{self.class.name}] Bot user ID is #{context.bot_namespace_id} with " \
           "username #{context.bot_namespace_name}."
    end

    def fetches_remote_project
      puts "[#{self.class.name}] Checking project ID #{context.id} ..."
      context.remote_project = context.gitlab.project(context.id)

      project_namespace = context.remote_project.namespace
      context.remote_project_namespace_id = project_namespace.id
      context.remote_project_namespace_username = project_namespace.name
      puts "[#{self.class.name}] Remote project namespace ID is " \
           "#{context.remote_project_namespace_id} with name " \
           "#{context.remote_project_namespace_username}."
    end

    #
    # Search and return (forked) projects from the remote name.
    #
    # This method could raise the `Gitlab::AmbigousProjectName` exception in the
    # case the search returns many projects.
    #
    def fetches_forked_project_from!(name)
      # Insecure to me, better option see : https://github.com/NARKOZ/gitlab/issues/406
      puts "[#{self.class.name}] Search for Gitlab project with name #{name} " \
           '...'
      forked_projects = context.gitlab.project_search(name)

      puts "[#{self.class.name}] #{forked_projects.size} Gitlab projects " \
           "found with name #{name} across all namespaces"

      bot_forks = forked_projects.select do |project|
        project.namespace.path == ENV['BOT_NAMESPACE']
      end

      case bot_forks.size
      when 0
        project_names = forked_projects.collect do |project|
          "#{project.name} from the #{project.namespace.path} namespace"
        end

        context.fail!(
          error: "Project #{name.inspect} couldn't be found from the " \
                 "#{ENV['BOT_NAMESPACE'].inspect} namespace.\nThe namespace " \
                 "has #{forked_projects.size} forks with those names: " \
                 "#{project_names.join("\n")}"
        )
      when 1
        forked_project = bot_forks.first
        puts "[#{self.class.name}] Project fetched from already forked " \
             "projects. Forked project ID is #{forked_project.id}."
        forked_project
      else
        context.fail!(error: "#{bot_forks.size} ambigous projects found with " \
                             "the name #{name}")
      end
    end

    #
    # Try to fork the remote project.
    # If already forked, just fetch project
    #
    def fork_remote_project
      context.gitlab.create_fork(context.id)
    rescue Gitlab::Error::Conflict
      # Already forked previously, select project from botspace.
      fetches_forked_project_from!(context.remote_project.name)
    end
  end
end
