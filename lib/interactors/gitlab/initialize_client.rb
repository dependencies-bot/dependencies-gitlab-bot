require 'gitlab'

module Gitlab
  class InitializeClient
    include Interactor

    def call
      sanity_checks!

      context.gitlab = Gitlab.client(gitlab_client_configuration)
    end

    private

    def sanity_checks!
      return unless ENV['GITLAB_PRIVATE_TOKEN'].nil?

      context.fail!(error: 'Missing GITLAB_PRIVATE_TOKEN env variable. ' \
                           'Please set it as a protected variable in Gitlab.')
    end

    def gitlab_client_configuration
      {
        endpoint: 'https://gitlab.com/api/v4',
        private_token: ENV['GITLAB_PRIVATE_TOKEN']
      }
    end
  end
end
