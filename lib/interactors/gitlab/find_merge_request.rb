module Gitlab
  class FindMergeRequest
    include Interactor

    def call
      sanity_checks!

      puts "[#{self.class.name}] Searching an existing merge request ..."
      context.merge_request = find_merge_request

      unless context.merge_request
        context.fail!(error: 'No opened merge request found')
      end

      # Let's continue the organizer's flow when the found merge request has a
      # state equal 'opened'.
      if context.merge_request.state == 'opened'
        puts "[#{self.class.name}] An existing merge request found in " \
             'opened state'
        return
      end

      puts "[#{self.class.name}] An existing merge request found but in " \
           "#{context.merge_request.state} state" \

      # Otherwise, if the merge request is closed or whatever else, just stop it
      context.fail!(error: 'No merge request found in the opened state')
    end

    private

    def sanity_checks!
      unless context.project_id
        context.fail!(error: { project_id: 'is missing' })
      end

      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.current_version
        context.fail!(error: { current_version: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      return if context.gitlab

      context.fail!(error: 'Gitlab client not initialized')
    end

    def find_merge_request
      puts "[#{self.class.name}] Searching a merge request in project with " \
           "ID #{context.project_id} with the \"#{build_title}\" title ..."
      context.gitlab.merge_requests(context.project_id).detect do |merge_request|
        puts "[#{self.class.name}] Merge request #{merge_request.title} ..."
        merge_request.title == build_title
      end
    end

    def build_title
      @title ||= Dependencies::GitlabBot::Helpers.build_merge_request_title_from(
        context.gem_name,
        context.current_version,
        context.next_version
      )
    end
  end
end
