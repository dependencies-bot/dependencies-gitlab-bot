module Gitlab
  class CreateMergeRequest
    include Interactor

    def call
      return if context.nothing_updated

      sanity_checks!

      puts "[#{self.class.name}] Creating merge request ..."
      # https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
      merge_request = context.gitlab.create_merge_request(
        context.local_project.id,
        build_title,
        description: build_description,
        remove_source_branch: true,
        source_branch: context.git_branch.name,
        target_branch: 'master',
        target_project_id: context.project_id
      )

      puts "[#{self.class.name}] Created merge request available at " \
           "#{merge_request.web_url}."
    end

    private

    def sanity_checks!
      unless context.project_id
        context.fail!(error: { project_id: 'is missing' })
      end

      unless context.local_project
        context.fail!(error: { local_project: 'is missing' })
      end

      unless context.gem_name
        context.fail!(error: { gem_name: 'is missing' })
      end

      unless context.current_version
        context.fail!(error: { current_version: 'is missing' })
      end

      unless context.next_version
        context.fail!(error: { next_version: 'is missing' })
      end

      unless context.git_branch
        context.fail!(error: { git_branch: 'is missing' })
      end

      return if context.gitlab

      context.fail!(error: 'Gitlab client not initialized')
    end

    def build_description
      build_title
    end

    def build_title
      Dependencies::GitlabBot::Helpers.build_merge_request_title_from(
        context.gem_name,
        context.current_version,
        context.next_version
      )
    end
  end
end
