module Gitlab
  class CheckMergeRequestStatus
    include Interactor

    def call
      sanity_checks!

      # Let's continue the organizer's flow when the merge request has an issue
      return if context.merge_request.merge_status == 'cannot_be_merged'

      # Otherwise, if the merge request is just pending, stops the organizer
      # execution
      context.fail!(error: "The merge request doesn't need any maintenance " \
                           "(status: #{context.merge_request.merge_status})")
    end

    private

    def sanity_checks!
      return if context.merge_request

      context.fail!(error: { merge_request: 'is missing' })
    end
  end
end
