require_relative 'bundler/update_gemfile'
require_relative 'bundler/update_gemfile_lock'
require_relative 'git/create_branch'
require_relative 'git/commit_and_push'
require_relative 'gitlab/create_merge_request'

class OrganizeUpdatingProjectGem
  include Interactor::Organizer

  organize Git::CreateBranch,
           Bundler::UpdateGemfile,
           Bundler::UpdateGemfileLock,
           Git::CommitAndPush,
           Gitlab::CreateMergeRequest
end
